from django.urls import path
from app.views import home, app

urlpatterns = [
    path('', home, name='home'),
    path('app/hello/', app, name='app')
]
