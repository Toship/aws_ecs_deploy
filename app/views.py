from django.shortcuts import render, HttpResponse


# Create your views here.

def home(request):
    print(request.headers)
    return render(request, 'home.html')


def app(request):
    return HttpResponse("APP")
