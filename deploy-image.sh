#!/bin/bash

# Variables
# TODO Change vairables
REPO_URI="320133736169.dkr.ecr.ap-south-1.amazonaws.com/jama-django"
LOCAL_IMAGE_NAME="jama-django:latest"
CLUSTER_NAME="ref-jamawealth-com"
TASK_NAME="jama-django-ec2"
CONTAINER_NAME="jama-django"
CONTAINER_PORT="80"
HOST_PORT="0"
SERVICE_NAME="jama-django"

# Check if docker is available
if command -v docker >/dev/null 2>&1; then
  echo "docker found"
  echo "version: $(docker -v)"
else
  echo "ERROR: docker not found."
  exit 1
fi

# login to ECR
echo "Logging in to ECR..."
$(aws ecr get-login --region ap-south-1 --no-include-email)

# Create Version
echo "Creating version..."
REPO_NAME=$(echo $REPO_URI | cut -d '/' -f 2)
TODAY=$(date --date='+5 hour 30 minutes' '+%Y%m%d')
LATEST_IMAGE_TAG="$(aws ecr describe-images --repository-name $REPO_NAME --output text --query 'sort_by(imageDetails,& imagePushedAt)[*].imageTags[*]' | tr '\t' '\n' | tail -1)"
if [ "$TODAY" == "$(echo $LATEST_IMAGE_TAG | cut -d '-' -f 1)" ]; then
  TAG_NO=$(echo "$LATEST_IMAGE_TAG" | cut -d '-' -f 2)
  VERSION="$TODAY-$(printf "%02d" $(($TAG_NO + 1)))"
else
  VERSION="$TODAY-01"
fi
echo "Version: $VERSION"

# push image to ECR
echo "Pushing image to ECR..."
docker tag $LOCAL_IMAGE_NAME "$REPO_URI:$VERSION"
docker push "$REPO_URI:$VERSION"
if [ $? -ne 0 ]; then
  echo "ERROR: Error while pushing the image"
  exit 1
fi

# create ECS task and update service
aws ecs register-task-definition --task-role-arn "arn:aws:iam::320133736169:role/ecsTaskExecutionRole" \
--network-mode "awsvpc" \
--family $TASK_NAME --container-definitions "[
    {
      \"portMappings\": [
        {
          \"hostPort\": $HOST_PORT,
          \"protocol\": \"tcp\",
          \"containerPort\": $CONTAINER_PORT
        }
      ],
      \"environment\": [
        {
          \"name\": \"DJANGO_SETTINGS_MODULE\",
          \"value\": \"app.settings.production\"
        }
      ],
      \"image\": \"$REPO_URI:$VERSION\",
      \"cpu\": 0,
      \"memoryReservation\": 400,
      \"name\": \"$CONTAINER_NAME\"
    }
  ]"
if [ $? -eq 0 ]; then
  aws ecs update-service --cluster $CLUSTER_NAME --service $SERVICE_NAME --task-definition $TASK_NAME
  if [ $? -ne 0 ]; then
    echo "ERROR: Error while updating service."
    exit 1
  else
    echo "Successfully deployed."
  fi
else
  echo "ERROR: Error while creating task."
  exit 1
fi
