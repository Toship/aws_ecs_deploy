#!/usr/bin/env bash
# start-server.sh
#if [ -n "$DJANGO_SUPERUSER_USERNAME" ] && [ -n "$DJANGO_SUPERUSER_PASSWORD" ] ; then
#	(python manage.py createsuperuser --no-input)
#fi
(gunicorn AWS_ECS_Deploy.wsgi --env DJANGO_SETTINGS_MODULE="AWS_ECS_Deploy.settings.$1" --user www-data --bind 0.0.0.0:8010 --workers 2) &
nginx -g "daemon off;"
