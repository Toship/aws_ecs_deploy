#!/bin/bash

# Variables
# TODO: Change repo link, image name and directory name
DIRECTORY="jamawealth-node"
REPO_LINK="git@bitbucket.org:ramkalyan/jama_wealth.git"
IMAGE_NAME="jamawealth-node"
CONTAINER_NAME="jamawealth"
CONTAINER_PORT="130"
HOST_PORT="130"


# Check if git and docker is installed
if command -v git >/dev/null 2>&1; then
  echo "git found"
  echo "version: $(git --version)"
else
  echo "ERROR: git not found."
  exit 1
fi
if command -v docker >/dev/null 2>&1; then
  echo "docker found"
  echo "version: $(docker -v)"
else
  echo "ERROR: docker not found."
  exit 1
fi


# Getting code from repository
for ((i = 0; i < 3; i++)); do
  if [ -d "$DIRECTORY" ]; then
    echo "Pulling repository..."
    (
      cd $DIRECTORY || exit 1
      git reset --hard
      git pull
    )
  else
    echo "Cloning the repository..."
    git clone $REPO_LINK $DIRECTORY
  fi
  if [ $? -eq 0 ]; then
    break
  else
    echo "Try again..."
  fi
done
if [ $? -ne 0 ]; then
  echo "Exceeded maximum number of retries."
  echo "ERROR: Couldn't pull the code from repository."
  exit 1
fi


# build docker image
#read -p "Enter version number [latest]: " VERSION
VERSION=${VERSION:-latest}
cd $DIRECTORY || exit 1
docker build -t "$IMAGE_NAME:$VERSION" .

if [ $? -eq 0 ]; then
  docker image prune -f
  RUNNING_CONTAINER="$(docker ps | grep 0.0.0.0:$HOST_PORT | awk '{print $1}')"
  if [ "$RUNNING_CONTAINER" != "" ]; then
    echo "Container $RUNNING_CONTAINER is already using port."
    echo "Stoping container $RUNNING_CONTAINER..."
    docker stop "$RUNNING_CONTAINER"
    echo "Stoped container $RUNNING_CONTAINER"
  fi
  docker run -p "$HOST_PORT:$CONTAINER_PORT" -d --name $CONTAINER_NAME --rm "$IMAGE_NAME:$VERSION"
  if [ $? -eq 0 ]; then
    echo "Docker container deployed successfully."
    exit 0
  fi
else
  echo "Build failed."
  exit 1
fi
